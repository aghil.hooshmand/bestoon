from django.shortcuts import render
from django.http import JsonResponse
from json import  JSONEncoder
from django.views.decorators.csrf import  csrf_exempt
from web.models import User,Token,Expense,Income
from datetime import  datetime


# Create your views here.
@csrf_exempt
def submit_expense(request):
    """Submit Expense"""
    #TODO validate data , user might be fake,token might be fake
    this_token=request.POST['token']
    this_user=User.objects.filter(token__token=this_token).get()
    now=datetime.now() #TODO; user might to submit to date herself
    Expense.objects.create(user=this_user,amount=request.POST['amount'],text=request.POST['text'],date=now)
    return(JsonResponse({'status':"OK"},encoder=JSONEncoder))

@csrf_exempt
def submit_income(request):
    """Submit Expense"""
    #TODO validate data , user might be fake,token might be fake
    this_token=request.POST['token']
    this_user=User.objects.filter(token__token=this_token).get()
    now=datetime.now() #TODO; user might to submit to date herself
    Income.objects.create(user=this_user,amount=request.POST['amount'],text=request.POST['text'],date=now)
    return(JsonResponse({'status':"OK"},encoder=JSONEncoder))